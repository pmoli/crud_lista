import React from "react";

import Taula from "./Taula";

export default (props) => {
  const columnes = [
    {
      nom: "nom",
      titol: "Nom",
    },
    {
      nom: "email",
      titol: "Correu electrònic",
    },
    {
      nom: "tel",
      titol: "telefon",
    },
    {
      nom: "alta",
      titol: "data alta",
    },
  ];

  return (
    <>
      <h3>Listado</h3>
      <Taula
        datos={props.data}
        columnas={columnes}
        rutaShow="/contactos/detalle/"
        rutaEdit="/contactos/editar/"
        rutaNuevo="/contactos/nuevo/"
      />
    </>
  );
};
