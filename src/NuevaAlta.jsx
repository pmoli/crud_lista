import React, { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import { FormGroup, Label, Input } from 'reactstrap';

export default (props) => {

    const [nom, setNom] = useState('');
    const [email, setEmail] = useState('');
    const [tel, setTel] = useState('');
    const [volver, setVolver] = useState(false);
    const [cuentaId,setCuentaId] = useState(props.lista.length)
    const crear = () => {
    const id = cuentaId + 1
    const alta = new Date().toJSON().slice(0,10)
 
    setCuentaId(id)
        const clienteNuevo = {
            id ,
            nom,
            email,
            tel,
            alta,

        };
        const nuevaLista = [...props.lista,clienteNuevo]
        props.setLista(nuevaLista);
        console.log(props.lista)
        setVolver(true)
        
    }

    if (volver) {
        return <Redirect to="/contactos" />
    }

    return (
        <>
            <h3>Nuevo</h3>
            <hr />

            <FormGroup>
                <Label for="nom">Nombre</Label>
                <Input type="text" name="nom" id="nom" value={nom} onChange={(e) => setNom(e.target.value)}/>
            </FormGroup>

            <FormGroup>
                <Label for="email">Email</Label>
                <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} />
            </FormGroup>

            <FormGroup>
                <Label for="tel">Tel</Label>
                <Input type="text" name="tel" id="tel" value={tel} onChange={(e) => setTel(e.target.value)} />
            </FormGroup>

            <hr />
            <Link className='btn btn-primary' to='/contactos' >Volver</Link>
            {' '}
            <button className='btn btn-success' onClick={crear} >Crear</button>
        </>
    );
};
