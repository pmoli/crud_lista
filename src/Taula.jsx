import React from "react";
import { Table } from "reactstrap";
// import styled from 'styled-components';
import {
  Link,
} from "react-router-dom";

// const ClicableTh = styled.th`
//     cursor: pointer;
//     :hover {
//         text-decoration: underline;
//         color: red;
//     }
// `;

export default (props) => {

  const filas = props.datos.map((el) => (
    <tr key={el.id}>
      {props.columnas.map((col, idx) => <td key={idx}>{el[col['nom']]}</td>)}
      <td><Link className="btn btn-success btn-sm" to={props.rutaShow + el.id} ><i className="fa fa-eye" ></i></Link></td>
      <td><Link className="btn btn-primary btn-sm" to={props.rutaEdit + el.id} ><i className="fa fa-edit " ></i></Link></td>
    </tr>
  ));

  return (
    <>
    <Table striped>
      <thead>
        <tr>
          {props.columnas.map((el, idx) => <th key={idx}>{el['titol']}</th>)}
          <th></th>
          <th></th>
        </tr>
      
      </thead>
      <tbody>{filas}</tbody>
      <tfoot>

      </tfoot>
      
    </Table>
    <Link className="btn btn-primary btn-sm" to={props.rutaNuevo} >Nuevo</Link>
    </>

  );
};
