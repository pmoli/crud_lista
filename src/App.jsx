import React, {useState, useEffect} from "react";
import {
  BrowserRouter,
  NavLink,
  Switch,
  Route,
} from "react-router-dom";
import { Container } from "reactstrap";

import Home from "./Home";
import Detalle from "./Detalle";
import Edita from "./Editar";
import Lista from "./Lista";
import NotFound from "./P404";
import NuevaAlta from "./NuevaAlta";

import data from "./agenda.json";

import "bootstrap/dist/css/bootstrap.min.css";

export default () => {

  const [lista, setLista] = useState(data);
  const guardar = () => {
    localStorage.setItem("mi_lista", JSON.stringify(lista));
  };

  const recuperar = () => {
    const itemsJson = localStorage.getItem("mi_lista");
    const cosas = JSON.parse(itemsJson);
    if (cosas && cosas.length) {
      setLista(cosas);
    } else {
      setLista([]);
    }
  };

  useEffect(()=>{
    recuperar()
  },[])

  useEffect(()=>{
    if(lista.length){
      guardar();
    }
  },[lista]);

  return (
    <BrowserRouter>
      <Container>
        <br />
        <ul className="nav nav-tabs">
          <li className="nav-item">
            <NavLink exact className="nav-link" to="/">
              Home
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/contactos">
              Contactos
            </NavLink>
          </li>
        </ul>
        <br />
        <br />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/contactos" render={() => <Lista data={lista} />} />
          <Route path="/contactos/detalle/:id" render={(props) => <Detalle {...props} data={lista} />} />
          <Route path="/contactos/editar/:id" render={(props) => <Edita {...props} data={lista} setLista={setLista} />} />
          <Route path="/contactos/nuevo" render={() => <NuevaAlta lista={lista} setLista={setLista} />} />
          
          <Route component={NotFound} />

        </Switch>
      </Container>
    </BrowserRouter>
  );
};
